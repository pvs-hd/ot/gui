# Package

version       = "0.1.0"
author        = "Andreas Eller, Alexandru Stefan Iov, Jonas Roos, Kilian Folger"
description   = "GUI for OpenTable"
license       = "MIT"
srcDir        = "src"
bin           = @["opentable", "databasesetup"]


# Dependencies

requires "nim >= 1.4.6"
requires "regex >= 0.19.0"
requires "result >= 0.2.0"

# for Neel
requires "jester >= 0.5.0"
requires "ws > 0.4.2"

# other OT sub-projects
requires "https://gitlab.com/pvs-hd/ot/backend.git >= 0.3.0 "
requires "https://gitlab.com/pvs-hd/ot/diesl.git >= 1.1.1"

task gendoc, "generates html documentation for the backend modules":
    exec("nimble doc --project --git.url:https://gitlab.com/pvs-hd/ot/gui/ --git.commit:master --git.devel:master --outdir:doc/html_doc doc/documentation.nim")
    mvFile("doc/html_doc/documentation.html", "doc/html_doc/index.html")
