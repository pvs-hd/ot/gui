# The OpenTable GUI

## Backend documentation
For the documentation of the backend logic used to communicate with other modules, see https://pvs-hd.gitlab.io/ot/gui/documentation/

## How to run OpenTable
Simply run `./build.py` (do `./build.py help to see all options`), then `./opentable[.exe]` to launch the program.
`./build.py setup-database` will delete and re-initialize the test database. Probably a good idea before doing any tests, although it presently works fine just clobbering existing tables.

Run `./build.py wipe` to delete all artifacts of building including npm files if something should go wrong
with building the front-end

### Requirements
- nodejs/npm
- nim compiler + nimble (obtained via [choosenim](https://github.com/dom96/choosenim), use chocolatey to get it on windows)

## Structure
- all nim language modules and backend logic are contained in [src](src)
- all frontend svelte components are contained in [svelte/src](svelte/src)
- all frontend logic for communicating with the backend and other modules is contained in [scripts](svelte/scripts), see [controller.js](svelte/scripts/controller.js) specifically for the last step in communication
- any test data and all database related files are contained in [data](data)

## How the frontend works
<div align="center">
![The frontned logic structure](doc/Frontend_Logic.png)
</div>

The frontend logic is separated into four parts: the svelte component logic, the [backend conversation manager](svelte/src/scripts/conversations.js), the [backend conversation controller](svelte/src/scripts/controller.js) and the [stores](svelte/src/scripts/stores.js). See the next section about adding a new instance of backend communication for more detail on the code.

- The only role svelte components play in the logic is to either define listeners or dispatch custom events. The rest is handled by the [conversation manager](svelte/src/scripts/conversations.js) and [controller](svelte/src/scripts/controller.js)
- Once a component dispatches a custom event to its parent (this should be [App.svelte](svelte/src/App.svelte) for the sake of cleanliness, but any parent is possible), the parent imports the handler function from the [controller](svelte/src/scripts/controller.js) and calls it
- Inside the [controller](svelte/src/scripts/controller.js), every handler function simply passes on parameters to a [message sending function](svelte/src/scripts/controller.js#L37) which uses the [conversation manager](svelte/src/scripts/conversations.js) to communicate with the backend
- Once a response is received, the [controller](svelte/src/scripts/controller.js) decides what needs changing in the GUI based on the response's structure and sets the values of the corresponding [stores](svelte/src/scripts/stores.js)
- Svelte components subscribe to the necessary [stores](svelte/src/scripts/stores.js) to display the data inside them

## Adding new backend communication instances to the frontend
Adding to the structure of the frontend is relatively streamlined, as it follows the steps described above. To start, one might either want to define a new component or add backend communication to an already existing one. If the former is chosen, make sure to create your new component in [svelte/src](svelte/src).

### Dispatching a custom event from your component
Once a component is created, a custom event dispatcher as well as an instance where that dispatcher is called should be defined inside it:
```
<script>
  import { createEventDispatcher } from "svelte"

  let my_event_dispatcher = createEventDispatcher()

  function dispatchMyEvent(){
    my_event_dispatcher("eventdispatched")
  }
  </script>

<button on:click={dispatchMyEvent}>Click here to talk to the backend</button>
```

### Adding the communication logic to the controller
Every custom event needs its own handler function in [controller.js](svelte/src/scripts/controller.js) which forwards parameters to [sendLSPRequestFromFrontend](svelte/src/scripts/controller.js#L37), as well as a case for its own response structure(this is mocked at the moment, proper decision-making will be added once the core module settles on a response format) and a resolve function:
```
function handleMyEvent() {
  sendLSPRequestFromFrontend("<the core function you are targeting>", {possible_parameter: value})
}

async function sendLSPRequestFromFrontend(method, parameters, timeout) {
    var response = await neel.sendLspRequest(method, parameters, timeout)
    
    switch(method) {
        case "core/table/exportCurrentTable":
            resolveExportTable(response)
            break;
        case "diesl/applyExpr":
            resolveDieslExpression(response)
            break;
        case "core/table/uploadTable":
            resolveCreateOrReloadTable(response)
            break;
        case "core/table/tableExists":
            return response["tableExists"]
        case "<the core function you are targeting>": <-------- add a case for your new communication instance
            resolveMyEvent()
            break;
        default:
            alert("Something went wrong! Serves you right for thinking this was a high quality program.")
    }
}

function resolveMyEvent() {
  --your code about what changes upon a succesful response goes here--
}
```

### Binding the event handler to the custom event
Once the custom event dispatcher and the handler are in place, the parent of the component dispatching the event (as mentioned before, try and keep this to[App.svelte](svelte/src/App.svelte)) needs to make the connection between the two:
```
import {handleMyEvent} from "./scripts/controller"

<Component on:eventdispatched={handleMyEvent}>
```

### Setting and subscribing to stores
More often than not, communicating with the backend will result in some form of change in the data displayed by the GUI. To account for such a case, one must define a new store in the [stores.js](svelte/src/scripts/stores.js) file (or use an already existing one):
```
export const my_new_store = writable(0)
```
Once defined, the store can be imported inside [controller.js](svelte/src/scripts/controller.js) and set using the resolve function of the new event:
```
import {my_new_store} from from "./stores"

function resolveMyEvent(new_data) {
  my_new_store.set(new_data)
}
```
The store can then be subscribed to by svelte components and used to display data, thus closing the frontend-to-backend cycle:
```
<script>
  import {my_new_store} from "./stores"

  let data_to_display

  my_new_store.subscribe(value => {
    data_to_display = value;
  })
  </script>

<p>{data_to_display}</p>
```

## Work distribution
- Kilian Folger:
  * Implemented a neel application in backend
  * Set up LSP communication to core to replace hardcoded actions
  * LSP conversation manager in the frontend that abstracts sending of LSP requests
  * CI/CD, Python build script that performs build/clean/setup tasks, some unit tests
- Alexandru Stefan Iov:
  * Set up direct frontend communication with the backend
  * Set up displaying of data received from the backend
  * Replaced hardcoded backend communication with a component-controller-conversation-store pipeline functioning with the LSP conversation manager
  * Readme, most of documentation automation, documentation deployment
- Jonas Roos:
  * Implemented the visible Elements of the GUI in HTML and CSS
  * Javascript functions for styling, like opening new windows or writing in a component (except the Table.svelte component)
  * First working version of the Terminal (was improved and adjusted by Alexandru Stefan Iov)
  * Partially set up the package.json for the needed packages and compiling of the CSS
- Andreas Eller:
  * Overlook design 
  * Support implementing the design in CSS and HTML
  * Introduction to Material UI
  * Presentation and inter group communication
  * Early structure development
