import { writable } from 'svelte/store';

//Terminal
export const terminal_content = writable('Welcome User for a list of all commands write :help. \n')

//Table
export const table_name = writable('')
export const table_columns = writable([])
export const table_rows = writable([])

export const selected_cell = writable({
    row: -1, 
    column: 'undefined'
})

//Theme
export const theme = writable("light");

//Loading
export const loading = writable('false');

//Upload Window
export const window = writable("false");