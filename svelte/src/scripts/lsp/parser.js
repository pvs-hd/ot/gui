/**
 * Wicked awesome parsing module!
 */
import {string, anyChar, int, rest, state, result} from "parjs";
import {backtrack, not, qthen, many, stringify, must, then, map, manyTill,
        replaceState, or, thenq, thenPick, each} from "parjs/combinators";

// The defined LSP headers, mapped to a Parjser that parses them. Handy!
/**
 * An object mapping the allowed LSP headers to Parjsers that parse them.
 */
export const headers =
  { "Content-Type": string("\r\n").pipe(backtrack(), not(), qthen(anyChar()))
                                  .pipe(many(), stringify()),
    "Content-Length": int().pipe(must(
      n => n > 0
        ? true
        : ({"kind":   "Hard",
            "reason": "negative Content-Length"})))};
export const requiredHeaders = [ "Content-Length" ]

export function lspMessage(){
  return lspHeaders().pipe(
    then(lspContent()),
    map(([message, content]) => {
      message["content"] = content;
      return message; }));}

export function lspHeaders(){
  let headersVerified = verifyAndStoreHeader().pipe(
    manyTill("\r\n"),
    qthen(requiredHeadersPresent()),
    map(st => st["headers"]));
  return replaceState({"headers": {}})(headersVerified)}

export function lspContent(){
  return rest().pipe(
    map((body) => {
      try                 { return { "result": JSON.parse(body) }; }
      catch (jsonParseErr){ return ({ "error": jsonParseErr });}}),
    must((result) => result["result"]
         ? true
         : ({"kind": "Hard",
             "reason": result["error"].toString()})),
    map(result => result["result"]));}

// Parses a string that is key of a header
function headerKey(){
  return Object.getOwnPropertyNames(headers)
    .map(string)
    .reduce((p1, p2) => p1.pipe(or(p2)))
    .pipe(or(string("")
             .pipe(must(x => ({ kind: "Soft",
                                reason: "expecting header key"})))))};

// Parses one LSP header line, including the "\r\n" at the end.
function header(){
  return headerKey().pipe(
    thenq(string(": ")),
    thenPick(key => headers[key].pipe(map(val => ({"key": key,
                                                   "value": val})))),
    thenq("\r\n"));}

// Parse an LSP header, store it in the parser's state, and crash if it was
// already present. This allows us to reject messages with duplicate headers.
// requires a state object with a "headers" member.
function verifyAndStoreHeader(){
  return header().pipe(
    thenPick(h => state().pipe(
      must(st => st["headers"][h["key"]]
           ? ({"kind":   "Hard",
               "reason": "duplicate header: " + h["key"]})
           : true),
      qthen(result(h)))),
    each((kv, st) => st["headers"][kv["key"]] = kv["value"]));}

// After reading all headers, check the state and verify that the required ones
// are present. As of now, that's only Content-Length.
// requires a state object with a "headers" member.
function requiredHeadersPresent(){
  return state().pipe(
    must(st => requiredHeaders.reduce(
      (trueOrError, header) =>
        (trueOrError === true &&!(st["headers"][header]))
        ? ({"kind":   "Hard", "reason": "missing header " + header})
        : trueOrError,
      true)));} // remember, only "true" means the result is accepted
