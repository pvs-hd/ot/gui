import { terminal_content, table_name, table_columns, table_rows, selected_cell, loading } from "./stores"

export function handleDieslExpression(event) {
    sendLSPRequestFromFrontend("diesl/applyExpr",
                               { "expr":event.detail.terminal_input.toString()
                               },
                               -1 ); // takes ages, can't handle timeout
}

export function handleExportTable() {
    sendLSPRequestFromFrontend("core/table/exportCurrentTable", { "format": "csv" })
}

export async function handleUploadTable(event) {
    if (await sendLSPRequestFromFrontend("core/table/tableExists", { "tableName": event.detail.upload_name })){
        if (confirm("A table with name " + event.detail.upload_name +
                    " already exists. Overwrite?"))
            event.detail.upload_file.text().then(function (contents) {
                sendLSPRequestFromFrontend("core/table/uploadTable", {
                    "tableName": event.detail.upload_name,
                    "format": event.detail.upload_file,
                    "contents": contents,
                    "clobber": true
                }, 30000)
            });
    }
    else event.detail.upload_file.text().then(function (contents) {
        sendLSPRequestFromFrontend("core/table/uploadTable", {
            "tableName": event.detail.upload_name,
            "format": event.detail.upload_file,
            "contents": contents,
            "clobber": true
        }, 30000)
    });
}

async function sendLSPRequestFromFrontend(method, parameters, timeout) {
    var response = await neel.sendLspRequest(method, parameters, timeout)
    
    //This switch is just a placeholder for the decision-making function until Core provides a response format
    switch(method) {
        case "core/table/exportCurrentTable":
            resolveExportTable(response)
            break;
        case "diesl/applyExpr":
            resolveDieslExpression(response)
            break;
        case "core/table/uploadTable":
            resolveCreateOrReloadTable(response)
            break;
        case "core/table/tableExists":
            return response["tableExists"]
        default:
            alert("Something went wrong! Serves you right for thinking this was a high quality program.")

    }
}
/* Commented for now. This will be the final structure once Core provides their response format
function decideResponseObjective(response) {
    switch(response) {
        //code deciding what needs to happen in the GUI based on the response structure goes here
    }
}*/

function resolveExportTable(csv_to_export) {
    if (!csv_to_export)
        throw Error("expecting {result {name, contents}}] or error, got " +
        csv_to_export);
    else if (csv_to_export["error"])
        alert(csv_to_export.error.message);
    else
        setCsvToDownload(csv_to_export);
}

function setCsvToDownload(csv_to_export) {
    var temp_download_link = document.createElement("a");
    temp_download_link.download = csv_to_export["name"];
    temp_download_link.href = 'data:text/csv;charset=utf-8,' +
        encodeURI(csv_to_export["contents"]);
    temp_download_link.target = '_blank';
    temp_download_link.click();
}

function resolveDieslExpression(error_or_table) {
    if (!error_or_table)
        throw Error("expecting table or error, got " + error_or_table)
    else if (error_or_table["error"])
        terminal_content.update(n => n + error_or_table["error"]["message"] + "\n")
    else
        resolveCreateOrReloadTable(error_or_table)

    loading.update(() => "false")
}

function resolveCreateOrReloadTable(table) {
    if (table["error"])
        alert(table.error.message)
    else {
        parseColumnNamesFromJson(table);
        parseRowContentFromJson(table);
        table_name.set(table["name"]);
    }
    loading.update(() => "false")
}

function parseColumnNamesFromJson(table) {
    var temp_columns = []
    for (let column_name in table["columnNames"]) {
        temp_columns.push({
            key: table["columnNames"][column_name],
            title: table["columnNames"][column_name],
            value: v => v[table["columnNames"][column_name]],
            sortable: true,
        });
    }
    table_columns.set(temp_columns)
}

function parseRowContentFromJson(table) {
    var temp_rows = []
    for (let row in table["content"]) {
        let row_to_append = {}
        for (let column_name in table["columnNames"]) {
            row_to_append[table["columnNames"][column_name]] =
                table["content"][row][column_name]
        }
        temp_rows.push(row_to_append)
    }
    table_rows.set(temp_rows)
}
