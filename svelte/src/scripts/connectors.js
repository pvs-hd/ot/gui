export class BackendConnector {
  receiveMessage;
  constructor(receiveMessageCallback){
    // configure message source to call the CB when a message comes in
    this.receiveMessage = receiveMessageCallback;
  }
  setReceiveMessageCallback(receiveMessageCallback){
    this.receiveMessage = receiveMessageCallback;
  }
  // send a message
  sendMessage(resource, message){
    this.receiveMessage(message);
  }
  close(){
  }
}

export class NeelConnector extends BackendConnector {
  neelController;
  sendProcName;
  receiveProcName;
  constructor(neel, receiveMessageCallback, receiveProcName){
    super(receiveMessageCallback);
    this.receiveProcName = receiveProcName;
    // this intermediate container allows us to change the receive callback
    // without having to fiddle with neel any more.
    let conn = this;
    function receiveMessage(message){
      conn.receiveMessage(message);
    }
    // Our version of Neel has a container for procs to be exposed to callJs
    // calls without polluting the global namespace, but this way, this
    // function will work even with the unmodified version of neel.
    if (neel.exposeJs){
      neel.exposeJs(receiveMessage, receiveProcName);
    } else {
      window[receiveProcName] = receiveMessage;
    }
  }
  sendMessage(resource, message){
    neel.callProc(resource, message);
  }
  close(){
    if (neel.removeJs)
      neel.removeJs(this.receiveProcName);
  }
}
