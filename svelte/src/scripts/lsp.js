/**
 * Various static functionality for dealing with LSP and RPC messages
 * Todo: fields that have types are checked, but not much else.
 */
import {must} from "parjs/combinators";
import {lspMessage} from "./lsp/parser.js";

export class Lsp {

  static rpc_version = "2.0";
  static rpc_error_messages =
    {"-32600": "Invalid Request",
     "-32610": "Invalid Notification",
     "-32620": "Invalid Response",
     "-32630": "Not valid as Request, Response, or Notification"};

  static constructRpcRequest(id, method, parameters = {}){
    var constructed_message = {}
    constructed_message.jsonrpc = Lsp.rpc_version
    constructed_message.id = id;
    constructed_message.method = method.toString()
    if(Object.keys(parameters).length !== 0)
      constructed_message.params = JSON.parse(JSON.stringify(parameters));
    return Lsp.validateRpcRequest(constructed_message)
  }

  static constructRpcNotification(method, parameters = {}){
    var constructed_notification = {}
    constructed_notification.jsonrpc = Lsp.rpc_version
    constructed_notification.method = method.toString()
    if(Object.keys(parameters).length !== 0)
      constructed_notification.params = JSON.parse(JSON.stringify(parameters));
    return Lsp.validateRpcNotification(constructed_notification)
  }
  
  static constructRpcResponse(id, result){
    var constructed_response = {};
    constructed_response.jsonrpc = Lsp.rpc_version;
    constructed_response.id = id;
    constructed_response.result = result;
    return Lsp.validateRpcResponse(constructed_response);
  }

  static constructRpcErrorResponse(id, error){
    var result = {};
    result.jsonrpc = Lsp.rpc_version;
    result.id = id;
    result.error = error;
    return result;
  }

  static constructRpcError(id, error_code){
    var constructed_error = {}
    var error_object = {}
    error_object.code = error_code
    error_object.message = Lsp.rpc_error_messages[error_code]
    constructed_error.jsonrpc = Lsp.rpc_version
    constructed_error.error = error_object
    constructed_error.id = id;
    return constructed_error
  }

  /**
   * Attempt to validate an object as an RPC request, response, or
   * notification, returning the object itself if it's valid, or the error
   * that the request validator produced if it fails all three tests.
   */
  static validateRpcMessage(message){
    if (Lsp.validateRpcRequest(message)         !== message
        && Lsp.validateRpcResponse(message)     !== message
        && Lsp.validateRpcNotification(message) !== message)
      return Lsp.constructRpcError("-32630");
    else
      return message;
  }

  static validateRpcRequest(message){
    if (message["jsonrpc"] != Lsp.rpc_version
        || !Lsp.validateMethod(message["method"])
        || !Lsp.validateId(message["id"]))
      return Lsp.constructRpcError("-32600")
    else
      return message
  }

  static validateRpcNotification(notification){
    if (notification["jsonrpc"] != Lsp.rpc_version
        || !Lsp.validateMethod(notification["method"]))
      return Lsp.constructRpcError("-32610")
    //Any other error codes I need to handle here? The structure can handle many
    else
      return notification
  }

  static validateRpcResponse(response){
    if (response["jsonrpc"] != Lsp.rpc_version
        || !Lsp.validateId(response["id"])
        || !response["result"] && !response["error"]
        || response["result"] && response["error"])
      return Lsp.constructRpcError("-32620")
    // Todo: validate the error
    else
      return response
  }

  static validateMethod(method){
    return typeof(method) === "string";
  }

  static validateId(id){
    return typeof(id) === "string"
      || (typeof(id) === "number" && (id % 1 === 0));
  }

  //============================================================================
  // LSP-SPECIFIC FUNCTIONALITY
  /**
   * Parse an LSP message, validating its header format and RPC content.
   * Currently doesn't allow for the LSP-specific message schemas.
   */
  static parseLsp(input){
    let parser = lspMessage().pipe(
      must(res => res["content"] === Lsp.validateRpcMessage(res["content"])
           ? true
           : ({"kind": "Hard",
               "reason": JSON.stringify(
                 Lsp.validateRpcMessage(res["content"]))})));
    return parser.parse(input).value;
  }

  /**
   * Takes in an RPC message, either as an object or as string, and returns
   * the minimal LSP message (i.e. with Content-Length header but nothing else)
   */
  static minimalLsp(message){
    if (typeof(message) === "string")
      return ("Content-Length: " + message.length.toString() +
              "\r\n\r\n" + message);
    else if (typeof(message) === "object"){
      let msg = JSON.stringify(message);
      return ("Content-Length: " + msg.length.toString() +
              "\r\n\r\n" + msg);
    } else
      throw new TypeError("Expected string or object, got " + typeof(message));
  }
}
