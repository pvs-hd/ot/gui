/**
 * See LspConversationManager.sendLspRequest. That's basically the point of
 * this entire module.
 */
import {BackendConnector} from "./connectors.js";
import {Lsp} from "./lsp.js";

export const ERROR_CODE_GENERIC           = -31999;
export const ERROR_CODE_MALFORMED_MESSAGE = -31998;
export const ERROR_CODE_TIMEOUT           = -31997;

export class BackendConversation {
  #id;
  #timeoutId;
  #terminated;
  constructor(id, timeoutId=undefined){
    this.#id = id;
    if (timeoutId)
      this.#timeoutId = timeoutId;
    this.#terminated = false;
  }
  getId(){ return this.#id; }
  getTimeoutId(){ return this.#timeoutId; }
  clearTimeout(){
    if (this.#timeoutId){
      clearTimeout(this.#timeoutId);
      this.#timeoutId = undefined;
    }
  }
  getTerminated(){ return this.#terminated; }
  terminate(){ this.#terminated = true; }
  receiveResponse(...args){ }
}

/**
 * A Promise with public "resolve" and "reject" methods that can be called
 * programmatically and an ID so the dispatcher can find out which promise
 * a given message from back-end is intended for.
 */
export class BackendPromise extends BackendConversation {
  resolveFunction;
  rejectFunction;
  #promise;
  constructor(id, timeoutId=undefined){
    super(id, timeoutId);
    let np = this;
    let promise = new Promise(function(resolve, reject){
      np.resolveFunction = resolve;
      np.rejectFunction  = reject;
    });
    this.#promise = promise;
  }
  // overridden methods
  receiveResponse(response){
    this.resolve(response);
  }
  // new methods
  /**
   * getPromise returns the Promise object that callbacks can be attached to
   */
  getPromise(){ return this.#promise; }
  resolve(result){
    this.resolveFunction(result);
    super.terminate();
  }
  reject(error){
    this.rejectFunction(error);
    super.terminate();
  }
}

/**
 * Extends BackendPromise with features that check that its result is valid
 * LSP/JSON-RPC
 */
export class LspRequest extends BackendPromise {
  constructor(...args){
    super(...args);
  }
  receiveResponse(message){
    let response = Lsp.parseLsp(message).content;
    if (response.error)
      super.reject(response.error);
    else if (response.result)
      super.resolve(response.result);
    else
      super.reject({ code: ERROR_CODE_MALFORMED_MESSAGE,
                     message: "response contains neither result nor error" });
  }
}
/**
 * Tasks:
 *   - Send and manage LSP request/response pairs, provide an API for creating
 *     promises from them.
 *   - Manage IDs and timeouts of conversations
 *   - Be available for later conversation upgrades
 */
export class LspConversationManager{
  // storage of conversations: a simple object. We have to prefix our integral
  // IDs with "r" because object props can't start with a digit.
  #backendConn;
  #conversations;
  #idKey(id){
    return "r" + id.toString()
  }
  constructor(backendConnector){
    let cm = this;
    backendConnector.setReceiveMessageCallback(function(message){
      cm.receiveMessage(message);
    });
    this.#backendConn = backendConnector;
    this.#conversations = {};
    this.nextId = 0;
  }

  // Basic conversation management.
  genId(){
    let id = this.nextId;
    this.nextId = (this.nextId + 1) % (2 ** 32);
    return id;
  }
  addConversation(c){
    let idString = this.#idKey(c.getId());
    if (this.#conversations[idString])
      throw Error("conversation with id "+ c.getId() + " already exists.");
    this.#conversations[idString] = c;
  }
  getConversation(id){
    if (typeof id !== "number" || id % 1 !== 0)
      throw TypeError("expecting integer, got " + id)
    return this.#conversations[this.#idKey(id)];
  }
  deleteConversation(id){
    if (typeof id !== "number" || id % 1 !== 0)
      throw TypeError("expecting integer, got " + id)
    delete this.#conversations[this.#idKey(id)];
  }

  // Communication
  receiveMessage(response){
    let id = Lsp.parseLsp(response).content["id"];
    let p = this.getConversation(id);
    if (!p){
      this.errNoSuchConversation(id);
    } else {
      p.clearTimeout();
      p.receiveResponse(response);
      if (p.getTerminated())
        this.deleteConversation(id);
    }
  }
  errNoSuchConversation(id){
    // todo
    console.log("no such conversation: " + id.toString());
  }

  // LSP-specific functionality
  createLspRequest(id, timeout){
    let newRequest;
    if (timeout > 0){
      let cm = this;
      let timeoutId = setTimeout(function(){
        let errRpc =
            JSON.stringify({"jsonrpc": "2.0",
                            "id": id,
                            "error": {"code": ERROR_CODE_TIMEOUT,
                                      "message": "conversation timed out"}});
        cm.receiveMessage(Lsp.minimalLsp(errRpc));
      }, timeout)
      newRequest = new LspRequest(id, timeoutId);
    }
    else
      newRequest = new LspRequest(id);
    this.addConversation(newRequest);
    return newRequest;
  }

  /**
   * Send RPC request to back-end as LSP. ID and RPC version fields are
   *  automatically generated, all other fields must match the specification
   * @return a promise that resolves when the backendConnector receives a valid
   * LSP response, or rejects when it receives an "error" response.
   * @param {string} method - the RPC method
   * @param {Object} params - an object matching the RPC request.params spec
   * @param {number} timeout - the timeout duration of the request in ms \
   *   pass in -1 for no timeout. Default is 1000.
   */
  sendLspRequest(method, params={}, timeout=1000){
    let id = this.genId();
    let request = this.createLspRequest(id, timeout);
    let messageContent = JSON.stringify(
      { "jsonrpc": "2.0",
        "id": id,
        "method": method,
        "params": params });
    this.#backendConn.sendMessage("sendLspMessage",
                                  Lsp.minimalLsp(messageContent));
    return request.getPromise();
  }
}
