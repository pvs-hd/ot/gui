import css from 'rollup-plugin-css-only';
import svelte from 'rollup-plugin-svelte';
import { svelteSVG } from "rollup-plugin-svelte-svg";
import postcss from 'rollup-plugin-postcss';
import { babel } from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs'
import { nodeResolve } from '@rollup/plugin-node-resolve';

export default {
  input: 'src/main.js',
  output: {
    file: '../web/app.js',
    name: 'app',
    format: 'iife'
  },
  plugins: [
    svelte({}),
    svelteSVG(),
    css({output: 'app.css'}),
    postcss({
      extract: true,
      minimize: true,
      use: [
        [
          'sass',
          {
            includePaths: ['./src/theme', './node_modules'],
          },
        ],
      ],
    }),
    nodeResolve({ dedupe: ['svelte'],
                  mainFields: ['browser', 'jsnext:main']}),
    // these two are needed to bridge the gap between libraries' ES5-style
    // module.exports and svelte's ES6-style imports
    babel(),
    commonjs()
  ]
}
