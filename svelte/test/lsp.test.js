import {readFileSync} from "fs";

import {Lsp} from "../src/scripts/lsp.js";
import {lspMessage} from "../src/scripts/lsp/parser.js";

var rpcExamples = JSON.parse(readFileSync("./test/rpcExamples.json"));


// =============================================================================
// JSON-RPC
test("validateRpcRequest accepts valid request", function(){
  expect(Lsp.validateRpcRequest(rpcExamples.requestOk))
    .toBe(rpcExamples.requestOk);
});
test("validateRpcResponse accepts valid response", function(){
  expect(Lsp.validateRpcResponse(rpcExamples.responseErrOk))
    .toBe(rpcExamples.responseErrOk);
});
test("validateRpcNotification accepts valid notification", function(){
  expect(Lsp.validateRpcNotification(rpcExamples.notificationOk))
    .toBe(rpcExamples.notificationOk);  
});
test("validateRpcRequest rejects request with bad method type", function(){
  expect(Lsp.validateRpcRequest(rpcExamples.requestBadPropType))
    .not.toBe(rpcExamples.requestBadPropType);
});
test("validateRpcResponse rejects response with both result and error props",
     function(){
       expect(Lsp.validateRpcResponse(rpcExamples.responseResultAndError))
         .not.toBe(rpcExamples.responseResultAndError);
     });
test("validateRpcNotification rejects notification with bad prop", function(){
  expect(Lsp.validateRpcRequest(rpcExamples.notificationMissingProp))
    .not.toBe(rpcExamples.notificationMissingProp);
});

test("constructRpcRequest creates RPC request as expected", function(){
  expect(JSON.stringify(Lsp.constructRpcRequest(13, "core/table/getTable",
                                                { "session": "12bb34ef9c9a3100",
                                                  "tableName": "users"})))
    .toBe(JSON.stringify(rpcExamples.requestOk));
});

test("constructRpcErrorResponse creates RPC response as expected", function(){
  expect(JSON.stringify(
    Lsp.constructRpcErrorResponse(600, { "code": -31999, "message": "Oh no!"})))
    .toBe(JSON.stringify(rpcExamples.responseErrOk));
});

test("constructRpcNotification creates RPC notificationas expected", function(){
  expect(JSON.stringify(Lsp.constructRpcNotification("core/user/logout")))
    .toBe(JSON.stringify(rpcExamples.notificationOk));
});

// =============================================================================
// LSP
test("minimalLsp creates parsable LSP", function(){
  let contentStr = JSON.stringify(rpcExamples.requestOk);
  let message = Lsp.minimalLsp(contentStr);
  expect(Lsp.parseLsp(message)["Content-Length"]).toBe(contentStr.length);
});

test("parseLsp parses correct, complete LSP request", function(){
  let content = JSON.stringify(rpcExamples["requestOk"]);
  let msg = "Content-Type: application/opentable-jsonrpc;charset=utf-8\r\n" +
      "Content-Length: " + content.length.toString() + "\r\n\r\n" + content;
  let result = lspMessage().parse(msg);
  expect(result.kind).toBe("OK");
});

test("parseLsp rejects missing Content-Length", function(){
  let content = JSON.stringify(rpcExamples["responseErrOk"]);
  let msg = "Content-Type: application/opentable-jsonrpc;charset=utf-8\r\n\r\n"
      + content;
  expect(lspMessage().parse(msg).kind).toBe("Hard");
});

test("parseLsp rejects unknown header", function(){
  let content = JSON.stringify(rpcExamples["responseErrOk"]);
  let msg = "If-None-Match: 19f948d03aeb9\r\n" +
      "Content-Type: application/opentable-jsonrpc;charset=utf-8\r\n" +
      "Content-Length: " + content.length.toString() + "\r\n\r\n" + content;
  expect(lspMessage().parse(msg).kind).toBe("Soft");
});

test("parseLsp rejects bad JSON", function(){
  let content = rpcExamples.badJson;
  let msg =  "Content-Type: application/opentable-jsonrpc;charset=utf-8\r\n" +
      "Content-Length: " + content.length.toString() + "\r\n\r\n" + content;
  expect(lspMessage().parse(msg).kind).toBe("Hard");
});
