import unittest
import json
import results

import "../src/opentable/lsp/parser"

suite "test LSP parsing and validation":
  setup:
    let rpcRequestOk = """
{ "jsonrpc": "2.0",
  "id": 13,
  "method": "core/table/getTable",
  "params": { "session": "12bb34ef9c9a3100",
              "tableName": "users"}}"""
    let rpcRequestNotOk = """
{ "jsonrpc": "2.0",
  "id": 13,
  "method": 21,
  "params": { "session": "12bb34ef9c9a3100",
              "tableName": "users"}}"""
    let rpcRequestBadJson = """
{ "jsonrpc": "2.0",
  "id": 13, parse error,
  "method": "core/table/getTable",
  "params": { "session": "12bb34ef9c9a3100",
              "tableName": "users"}}"""

    proc contentLength(rpcBody : string) : string =
      "Content-Length: " & $(len rpcBody) & "\r\n\r\n" & rpcBody

    let lspRequestMinimal = contentLength rpcRequestOk
    let contentType = "application/lich-jsonrpc; charset=utf-8"
    let lspRequestCT = "Content-Type: " & contentType & "\r\n" &
      contentLength rpcRequestOk
    let lspRequestBadRpc = contentLength rpcRequestNotOk
    let lspRequestBadHeaderValue = "Content-Length: 2.72\r\n\r\n" & rpcRequestOk
    let lspRequestUnknownHeader = "If-Modified-Since: this morning\r\n" &
      contentLength rpcRequestOk
    let lspRequestBadCL = "Content-Length: 4096\r\n\r\n" & rpcRequestOk
    let lspRequestBadJson = contentLength rpcRequestBadJson
    let lspRequestLFOnly = "Content-Length: " & $(len rpcRequestOk) & "\n\n" &
      rpcRequestOk

  teardown:
    discard

  test "accept valid minimal request":
    let res = parseLsp lspRequestMinimal
    assert res.isOk
  test "accept valid request with Content-Type":
    let res = parseLsp lspRequestCT
    assert res.get.contentType == contentType
  test "reject bad RPC":
    let res = parseLsp lspRequestBadRpc
    assert res.error.kind == lekBadRpc
  test "reject bad header value":
    let res = parseLsp lspRequestBadHeaderValue
    assert res.error.kind == lekBadHeader
  test "reject unknown header":
    let res = parseLsp lspRequestUnknownHeader
    assert res.error.kind == lekBadHeader
  test "reject bad content-length":
    let res = parseLsp lspRequestBadCL
    assert res.error.kind == lekBadHeader
  test "reject bad JSON":
    let res = parseLsp lspRequestBadJson
    assert res.error.kind == lekBadJson
  test "reject LF-only line endings":
    let res = parseLsp lspRequestLFOnly
    assert res.error.kind == lekParseError
  test "reject headerless message":
    let res = parseLsp rpcRequestOk
    assert res.error.kind == lekParseError  
  test "reject empty string":
    let res = parseLsp ""
    assert res.error.kind == lekBadHeader
