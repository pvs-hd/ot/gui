import unittest
import json
import results
import "../src/opentable/lsp/rpc"

suite "test RPC validation":
  setup:
    let requestOk = parseJson """
{ "jsonrpc": "2.0",
  "id": 13,
  "method": "core/table/getTable",
  "params": { "session": "12bb34ef9c9a3100",
              "tableName": "users"}}
"""
    let requestBadPropType = parseJson """
{ "jsonrpc": "2.0",
  "id": 13,
  "method": 21,
  "params": { "session": "12bb34ef9c9a3100",
              "tableName": "users"}}
"""
    let responseOk = parseJson """
{ "jsonrpc": "2.0",
  "id": "14f9ad9e",
  "result": [1, 2, 3] }
"""
    let responseErrOk = parseJson """
{ "jsonrpc": "2.0",
  "id": 600,
  "error": { "code": -31999,
             "message": "Oh no!" }}
"""
    let responseMissingProp = parseJson """
{ "jsonrpc": "2.0",
  "params": {"session": "12bb34ef9c9a3100",
             "tableName": "users"}}
"""
    let responseResultAndError = parseJson """
{ "jsonrpc": "2.0",
  "id": "core/table/getTable",
  "result": {"session": "12bb34ef9c9a3100",
             "tableName": "users" },
  "error": { "code": -31999,
             "message": "Oh no!" }}
"""
    let responseNeitherResultNorError = parseJson """
{ "jsonrpc": "2.0",
  "id": "core/table/getTable" }
"""
    let notificationOk = parseJson """
{ "jsonrpc": "2.0",
  "method": "core/user/logout" }
"""
    let notificationInvalidProp = parseJson """
{ "jsonrpc": "2.0",
  "id": 13,
  "method": "core/table/getTable" }
"""
  teardown:
    discard

  test "accept valid request":
    let res = validateRequest requestOk
    assert res.isOk
  test "reject bad request property type":
    let res = validateRequest requestBadPropType
    assert res.isErr
    assert res.error.kind == rekBadPropType
  test "accept valid response":
    let res = validateResponse responseOk
    assert res.isOk
  test "accept valid error response":
    let res = validateResponse responseErrOk
    assert res.isOk
  test "reject missing response property":
    let res = validateResponse responseMissingProp
    assert res.isErr
    assert res.error.kind == rekMissingProp
  test "reject response with result and error":
    let res = validateResponse responseResultAndError
    assert res.isErr
    assert res.error.kind == rekOther
  test "reject response with neither result nor error":
    let res = validateResponse responseNeitherResultNorError
    assert res.isErr
    assert res.error.kind == rekMissingProp
  test "accept valid notification":
    let res = validateNotification notificationOk
    assert res.isOk
  test "reject invalid notification prop":
    let res = validateNotification notificationInvalidProp
    assert res.isErr
    assert res.error.kind == rekInvalidProp
  test "general validation function accepts request":
    assert rpc.validate(requestOk).isOk
  test "general validation function accepts response":
    assert rpc.validate(responseOk).isOk
  test "general validation function accepts notification":
    assert rpc.validate(notificationOk).isOk
  test "general validation function rejects invalid message":
    let res = rpc.validate responseNeitherResultNorError
    assert res.isErr
    assert res.error.kind == rekMissingProp
