import os, db_sqlite
from core/core/tablemanagement import writeNewTableToDatabase
from core/table import createTableFromCsv
import core/database


proc deleteExampleDatabase(dbFile : string) : void


if (paramCount() != 2):
  echo "Usage: databasesetup <database> <initcsv>"

var databaseFile = paramStr(1)
var csv = paramStr(2)

if fileExists databaseFile:
  deleteExampleDatabase databaseFile

initDatabase(databaseFile)
let db = open(databaseFile, "", "", "")
let t1  = createTableFromCsv csv
db.writeNewTableToDatabase t1
close db


proc deleteExampleDatabase(dbFile : string) : void =
  let db = open(dbFile, "", "", "")
  try:
    let query = "SELECT path FROM project"
    for projectdb in db.fastRows(sql query):
      removeFile projectdb[0]
  finally:
    close db
    removeFile databaseFile
