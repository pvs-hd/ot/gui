##[
    - Procedures called from an exposeProc'd procedure need to
    be either defined beforehand or marked GC-safe. The latter is fine, though,
    the compiler will figure out if you're lying.
    - Convention: all exposeProcs have as their first parameter a "jsCallback"
    field which is the name of a JS fn which will be applied to the result
]##
from sequtils import map
from sugar import `=>`
import macros
from options import Option, isSome, some, get
from locks import Lock, initLock, deinitLock, withLock
import logging except log
from json import `%`, getFields
import results
import db_sqlite

import neel except Table
import jester except Table

import core
import diesl
from diesl/operations/types import DieslDatabaseSchema
import diesl/compat/coretodiesl

import opentable/args
import opentable/lsp


#===============================================================================
# TYPES
type
  HardcodedLspDispatcher = ref object of LspDispatcher
  AppState = object
    masterDatabase : string
    currentProject : string
    currentTable   : string
    lspDispatcher  : LspDispatcher

proc `$`(appState : AppState) : string =
  result = "AppState(database: " & appState.masterDatabase &
    ", currentProject: " & appState.currentProject &
    ", currentTable: " & appState.currentTable &
    ", lspDispatcher: ...)"
# get string rep of current global app state
proc showAppState() : string {.gcsafe.}

#===============================================================================
# CONSTANTS
const ASSETS_DIR = "web"
const DEFAULT_PORT_NO = 5000
const DEFAULT_MASTER_DATABASE = "data/database/test.sqlite"

#===============================================================================
# PROCEDURES
# tier 1: used by exposeProcs (which shouldn't do anything else)
proc sendLspMessage*(message : string) : void {.gcsafe.}
proc downloadCurrentTable*() : JsonNode {.gcsafe.}
proc downloadTable*(tableName : string) : JsonNode {.gcsafe.}
proc downloadTables*(tableNames : seq[JsonNode]) : JsonNode {.gcsafe.}
proc loadTable*(tableName : string) : JsonNode {.gcsafe.}
proc tableExists*(tableName : string) : JsonNode {.gcsafe.}
proc uploadTable*(tableName : string, format : string,
                  contents : string, clobber : bool) : JsonNode {.gcsafe.}
proc applyDslExpr*(command: string) : JsonNode {.gcsafe.}
proc exportCurrentTable*(format: string) : JsonNode {.gcsafe.}

# tier 2 - used by the wrapped procs in tier 1
method sendMessage(disp : HardcodedLspDispatcher, message : LspMessage) :
       bool {.nimcall gcsafe.}
proc sendLspMessageToClient(message : LspMessage) : bool {.nimcall gcsafe.}

proc getTableOrErrorAsJson(tableName : string,
                           projectName : string = "default") :
                          JsonNode {.gcsafe.}
proc getCurrentProjectDBPath() : string {.gcsafe.}
proc getTable(tableName : string, projectName : string = "default") :
             Table {.gcsafe.}
proc getTableWithoutRowIds(tableName : string,
                           projectName : string = "default") :
                          Table {.gcsafe.}
proc getCurrentTable() : Table {.gcsafe.}
proc setCurrentTable(tableName : string, table : Table,
                     clobber : bool = false) : void {.gcsafe.}
proc setCurrentTableUnsafe(dbConn : DbConn, tableName : string, table : Table,
                           clobber : bool = false) : void {.gcsafe.}
proc transformCurrentTable(dieslCommand : string) : Table {.gcsafe.}

# more deeply nested helper procs
proc mkSchema(table : Table) : DieslDatabaseSchema {.gcsafe.}
proc dieslToSqlite(dieslCommand : string,
                   schema : DieslDatabaseSchema = DieslDatabaseSchema()) :
                     seq[SqlQuery] {.gcsafe.}
proc transformCurrentTable(sqlCommands : seq[SqlQuery]) : Table {.gcsafe.}

# logging/debug
proc upTo[T](seqLike : T, numItems : int) : T {.gcsafe.}
proc infoSetCurrentTableAlreadyPresent(tname : string, clobber : bool) :
                                      void {.gcsafe.}
proc raiseExceptionIfCurrentTableNotSet() : void {.gcsafe.}

#===============================================================================
# TEMPLATES AND MACROS
# turns out you can't forward declare templates or macros. phooey.
template log(level : Level, args : varargs[string, `$`]) =
  withLock loggerLock:
    logging.log(logger[], level, args)

macro assignValueOrErrorAsJson(varIdent : typed, body: typed) : void =
  ## perform `body` statements that produce a JSON node as a result, assigning
  ## it to `varIdent`, or assigning a JSON error object if an exception occurs.
  ## example usage:
  ## assignValueOrErrorAsJson response:
  ##   response = getTableAsJson("table1")
  let exceptBlock = quote do:
    var errName = $getCurrentException().name
    var errMsg = getCurrentExceptionMsg()
    var errInfo = %(@[("type", %errName),
                      ("message", %errMsg)])
    `varIdent` = %(@[("error", errInfo)])
  return nnkTryStmt.newTree(body, nnkExceptBranch.newTree exceptBlock)

macro returnValueOrErrorAsJson(body: typed) : void =
  ## perform `body` statements that return a JSON node, returning either the
  ## result or a JSON error object if an exception occurs. `body` must end with
  ## a `return` statement, an assignment to `result`, or a `raise` statement
  ## example usage:
  ## returnValueOrErrorAsJson:
  ##   return getTableAsJson("table1")
  let exceptBlock = quote do:
    var errName = $getCurrentException().name
    var errMsg = getCurrentExceptionMsg()
    var errInfo = %(@[("type", %errName),
                      ("message", %errMsg)])
    return %(@[("error", errInfo)])
  return nnkTryStmt.newTree(body, nnkExceptBranch.newTree exceptBlock)


#===============================================================================
# APP
let defaultArgs = Args(portNo: DEFAULT_PORT_NO,
                       database: DEFAULT_MASTER_DATABASE)
let programArgs : Args = parseArguments defaultArgs

# application state

# appStateTraced is not used, only the untraced pointer appState
var appStateLock : Lock
# check it out! you can do let closures with semicolons!
var appStateTraced : AppState =
  (let disp = HardcodedLspDispatcher(receiveMessage: sendLspMessageToClient);
       AppState(masterDatabase : programArgs.database,
                currentProject : "default",
                currentTable   : "",
                lspDispatcher  : disp))
# skipping guard pragma because it makes decomposing procs impossible.
# only the procs called directly from exposeProcs use the lock.
var appState : ptr AppState = appStateTraced.addr

# logging
# not sure if this actually needs guarded, but better be safe for now
var loggerLock : Lock
var loggerRef = newConsoleLogger()
var logger {.guard: loggerLock.} : ptr ConsoleLogger = loggerRef.addr

# init
initLock(loggerLock)
initLock(appStateLock)

exposeProcs:
  proc sendLspMessage(message : string) =
    log(lvlDebug, "callProc(\"sendLspMessage\", \"" & message & ")")
    opentable.sendLspMessage(message)
    
  proc downloadCurrentTable(jsCallback : string) =
    log(lvlDebug, "callProc(\"downloadCurrentTable\", \"" & jsCallback &
      "\"), currentTable=" & appState[].currentTable)
    callJs(jsCallback, opentable.downloadCurrentTable().getFields)

  proc downloadTable(jsCallback : string, tableName : string) =
    log(lvlDebug, "callProc(\"downloadTable\", \"" &
      jsCallback&"\", \"" & tableName&"\")")
    callJs(jsCallback, opentable.downloadTable(tableName).getFields)

  proc downloadTables(jsCallback : string, tableNames : seq[JsonNode]) =
    log(lvlDebug, "callProc(\"downloadTables\", \"" &
      jsCallback&"\", \"" & ($tableNames)&"\")")
    callJs(jsCallback, opentable.downloadTables(tableNames).getFields)

  proc loadTable(jsCallback : string, tableName : string) =
    log(lvlDebug, "callProc(\"loadTable\", \"" &
      jsCallback&"\", \"" & tableName&"\")")
    callJs(jsCallback, opentable.loadTable(tableName).getFields)

  proc tableExists(conversationId : int, tableName : string) =
    # First run of the new conversation logic!
    log(lvlDebug, "callProc(\"tableExists\", " &
      $conversationId & ", \"" & tableName & "\")")
    callJs("sendRpcMessage", conversationId,
           opentable.tableExists(tableName).getFields)

  proc uploadTable(jsCallback : string, tableName : string,
                   format : string, contents : string, clobber : bool) =
    log(lvlDebug, "callProc(\"uploadTable\", \"" & jsCallback&"\", \"" &
      tableName&"\", \""&format&"\",\"" & contents.upTo(64) &
      ", \"" & $clobber & "...\")")
    callJs(jsCallback, opentable.uploadTable(tableName, format,
                                             contents, clobber).getFields)

  proc applyDslExpr(jsCallback: string, command: string) =
    log(lvlDebug, "callProc(\"applyDslExpr\", \"" &
      jsCallback&"\", \""&command&"\")")
    callJs(jsCallback, opentable.applyDslExpr(command).getFields)

  proc exportCurrentTable(jsCallback : string, format : string) =
    log(lvlDebug, "callProc(\"exportCurrentTable\", \"" & jsCallback &
      "\", \"" & format & "\")")
    callJs(jsCallback, opentable.exportCurrentTable(format).getFields)

# Start app
let portNo = programArgs.portNo # must extract, or else startApp isn't gcsafe
startApp(startUrl="index.html", assetsDir=ASSETS_DIR,
         appMode=false, portNo=portNo)

deinitLock(appStateLock)
deinitLock(loggerLock)


#===============================================================================
# IMPLEMENTATIONS
# SERVER-CLIENT-COMMUNICATION
proc sendLspMessage*(message : string) : void {.gcsafe.} =
  let messageParsed : Result[LspMessage, LspError] = parseLsp message
  if messageParsed.isErr:
    log(lvlDebug, $messageParsed.error())
  else:
    var disp : LspDispatcher
    withLock appStateLock:
      disp = appState[].lspDispatcher
    discard disp.sendMessage(messageParsed.get())

proc downloadCurrentTable*() : JsonNode {.gcsafe.} =
  var table : Table
  withLock appStateLock:
    table = getCurrentTable()
  return %table

proc downloadTable*(tableName : string) : JsonNode {.gcsafe.} =
  ## Retrieve a table from the database and send it to client. Return true
  ## if successful, false if an exception was raised.
  withLock appStateLock:
    return getTableOrErrorAsJson(tableName, appState[].currentProject)
  
proc downloadTables*(tableNames : seq[JsonNode]) : JsonNode {.gcsafe.} =
  ## Retrieve a list of tables from the database and send it to client. Return
  ## true if successful, false if an exception was raised.
  returnValueOrErrorAsJson:
    withLock appStateLock:
      var tableList : seq[JsonNode]
      for node in tableNames:
        var name = node.getStr
        if name == "":
          raise newException(CatchableError,
                             "downloadTables: invalid table name: " & $node)
        tableList.add %getTable(name, appState[].currentProject)
      return %(@[("result", %tableList)])

proc loadTable*(tableName : string) : JsonNode {.gcsafe.} =
  var response : JsonNode
  withLock appStateLock:
    response = getTableOrErrorAsJson(tableName, appState[].currentProject)
    if not response.hasKey("error"):
      appState[].currentTable = tableName
  return response

proc tableExists*(tableName : string) : JsonNode {.gcsafe.} =
  returnValueOrErrorAsJson:
    withLock appStateLock:
      let dbConn = open(appState[].masterDatabase, "", "", "")
      try:
        let exists = dbConn.tableExists(tableName, appState[].currentProject)
        return %*{ "tableExists": exists,
                   "tableName": tableName }
      finally:
        close dbConn

proc uploadTable*(tableName : string, format : string,
                  contents : string, clobber : bool) : JsonNode {.gcsafe.} =
  var table = createTablefromCsvString(tablename = tableName,
                                       csvstring = contents)
  withLock appStateLock:
    setCurrentTable(tableName, table, clobber)
    table = getCurrentTable() # to make sure it's the exact in-DB representation
  return %table

proc applyDslExpr*(command: string) : JsonNode {.gcsafe.} =
  returnValueOrErrorAsJson:
    withLock appStateLock:
      var currentTable = transformCurrentTable command
      return  %currentTable

proc exportCurrentTable*(format : string) :
                       JsonNode {.gcsafe.} =
  returnValueOrErrorAsJson:
    raiseExceptionIfCurrentTableNotSet()
    withLock appStateLock:
      let currentTable = getTableWithoutRowIds(appState[].currentTable,
                                               appState[].currentProject)
      return  %(@[("name", %(currentTable.name & "." & format)),
                  ("contents", %(toCsvString currentTable))])


# Tier 2
method sendMessage(disp : HardcodedLspDispatcher, message : LspMessage) :
       bool =
  log(lvlDebug, "sendMessage")
  let content : JsonNode = message.content
  let params : JsonNode  = content["params"]
  if message.isNotification:
    log(lvlDebug, "received notification with method " & $content["method"])
    return false
  elif message.isResponse:
    log(lvlDebug, "received response with id " & $content["id"])
    return false
  else:
    case content["method"].getStr:
      of "core/table/getTable":
        echo "core/table/getTable"
      of "core/table/getTables":
        echo "core/table/getTables"
      of "core/table/tableExists":
        log(lvlDebug, "tableExists")
        let exists : JsonNode = tableExists(params["tableName"].getStr)
        log(lvlDebug, $exists)
        let response = %*{ "jsonrpc": "2.0",
                           "id": content["id"],
                           "result": { "tableExists": exists["tableExists"]}}
        log(lvlDebug, $response)
        discard disp.receiveMessage(minimalLspHeaders response)
      of "core/table/uploadTable":
        let tableJson =
          uploadTable(params["tableName"].getStr, params["format"].getStr,
                      params["contents"].getStr, params["clobber"].getBool)
        discard disp.receiveMessage(
          minimalLspHeaders (%*{ "jsonrpc": %"2.0",
                                 "id": content["id"],
                                 "result": tableJson }))
      of "diesl/applyExpr":
        let tableJson = applyDslExpr(params["expr"].getStr)
        discard disp.receiveMessage(
          minimalLspHeaders (%*{ "jsonrpc": %"2.0",
                                 "id": content["id"],
                                 "result": tableJson }))
      of "core/table/exportCurrentTable":
        let responseResult = exportCurrentTable(params["format"].getStr)
        discard disp.receiveMessage(
          minimalLspHeaders (%*{ "jsonrpc": %"2.0",
                                 "id": content["id"],
                                 "result": responseResult }))
      else:
        log(lvlDebug, "Unknown method: " & content["method"].getStr)
    return true
        
proc sendLspMessageToClient(message : LspMessage) : bool {.nimcall gcsafe.} =
  log(lvlDebug, "sendLspMessageToClient " & $message)
  callJs("sendLspMessage", $message)
  return true

proc getTableOrErrorAsJson(tableName : string,
                           projectName : string = "default") :
                          JsonNode {.gcsafe.} =
  # Get a table as JSON
  returnValueOrErrorAsJson:
    return %getTable(tableName, projectName)

proc getCurrentProjectDBPath() : string {.gcsafe.} =
  let db = open(appState[].masterDatabase, "", "", "")
  try:
    return db.getProjectInfo(appState[].currentProject).path
  finally:
    close db

proc getTable(tableName : string, projectName : string = "default") :
             Table {.gcsafe.} =
  # Get a table from DB if present, or raise exception if not.
  let dbConn = open(appState[].masterDatabase, "", "", "")
  try:
    var table = dbConn.getTableWithRowId(tableName, projectName)
    # we now have a table with a rowid at the end of each row, but no
    # appropriate column header.
    table.columnNames.add "rowid"
    table.columnTypes.add "integer"
    return table
  finally:
    close dbConn

proc getTableWithoutRowIds(tableName : string,
                           projectName : string = "default") :
                          Table {.gcsafe.} =
    let dbConn = open(appSTate[].masterDatabase, "", "", "")
    try:
      return dbConn.getTable(tableName, projectName)
    except:
      close dbConn

proc getCurrentTable() : Table {.gcsafe.} =
  ## Get the current table (as stored in appState[].currentTable) from the DB.
  raiseExceptionIfCurrentTableNotSet()
  return getTable(appState[].currentTable, appState[].currentProject)

proc setCurrentTable(tableName : string, table : Table,
                     clobber : bool = false) : void {.gcsafe.} =
  ## Store current table in database under a given name. If table is already
  ## present, ``clobber`` decides whether to overwrite it.
  log(lvlDebug, "setCurrentTable(" & tableName & "," & ($table).upTo(64) &
    "...," & $clobber)
  let dbConn = open(appState[].masterDatabase, "", "", "")
  try:
    if dbConn.tableExists tableName:
      infoSetCurrentTableAlreadyPresent(tableName, clobber)
    dbConn.setCurrentTableUnsafe(tableName, table, clobber)
  finally:
    close dbConn

proc infoSetCurrentTableAlreadyPresent(tname : string, clobber : bool) :
                                      void {.gcsafe.} =
  log(lvlInfo, "table " & tname & " already present.")
  if clobber: log(lvlInfo, "overwriting previous table")
  else:       log(lvlInfo, "clobber=false, doing nothing")
proc setCurrentTableUnsafe(dbConn : DbConn, tableName : string, table : Table,
                           clobber : bool = false) : void {.gcsafe.} =
  if (not dbConn.tableExists tablename):
    dbConn.writeNewTableToDatabase(table, appState[].currentProject)
    appState[].currentTable = tableName
  elif clobber:
    dbConn.deleteTable(tableName, appState[].currentProject)
    dbConn.writeNewTableToDatabase(table, appState[].currentProject)
    appState[].currentTable = tableName


# todo: use runScript's schema parameter to ensure this actually works on
# the current table
proc transformCurrentTable(dieslCommand : string) : Table {.gcsafe.} =
  ## Apply a DSL expression to the current table and return the altered table.
  log(lvlDebug, "transformCurrentTable " & dieslCommand)
  var sqliteCommands = dieslToSqlite(dieslCommand, mkSchema getCurrentTable())
  var sqliteCommandsString = $(sqliteCommands.map(
    proc(q : SqlQuery) : string = cast[string](q)))
  log(lvlDebug, "transformCurrentTable: exec " & sqliteCommandsString)
  return transformCurrentTable(sqliteCommands)

proc mkSchema(table : Table) : DieslDatabaseSchema {.gcsafe.} =
  toDieslDatabaseSchema(@[table])

proc dieslToSqlite(dieslCommand : string,
                   schema : DieslDatabaseSchema = DieslDatabaseSchema()) :
                  seq[SqlQuery] {.gcsafe.} =
  # runScript overwrites appState[].currentTable and it appears to be a deeply
  # rooted bug in the compiler lib, so we used a workaround.
  var currentState : AppState = appState[]
  var dieslOperations = runScript(dieslCommand, schema)
  appState[] = currentState
  return dieslOperations.toSqlite.map(str => sql(str))

proc transformCurrentTable(sqlCommands : seq[SqlQuery]) : Table {.gcsafe.} =
  raiseExceptionIfCurrentTableNotSet()
  let dbConn = open(getCurrentProjectDBPath(), "", "", "")
  try:
    for sqlCommand in sqlCommands:
      dbConn.exec(sqlCommand)
    var currentTable = getTable(appState[].currentTable,
                                appState[].currentProject)
    return currentTable
  finally:
    close dbConn

# logging/diagnostic
proc showAppState() : string {.gcsafe.} = $(appState[])

proc upTo[T](seqLike : T, numItems : int) : T {.gcsafe.} =
  if len(seqLike) <= numItems:
    return seqLike
  else:
    return seqLike[0..<numItems]
proc raiseExceptionIfCurrentTableNotSet() : void {.gcsafe.} =
  let currentTableName = appState[].currentTable
  if currentTableName == "":
    raise newException(CatchableError, "currentTable not yet set (==\"\")")
