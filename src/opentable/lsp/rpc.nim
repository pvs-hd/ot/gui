##[
Library for validating RPC requests. It's implemented using Result, which is
a fair amount of boilerplate but makes implementing stuff on top of it fairly
easy. And it's entirely functional, with all the advantages of that.
]##

from sequtils import map, foldl
import json
import results
import sugar

type
  JsonProp* = tuple[key : string, types : seq[JsonNodeKind], required : bool]
  ## Describes a property required of a specific RPC message format
  RpcErrorKind* = enum ## types of RPC validation errors
    rekMissingProp, rekBadPropType, rekInvalidProp, rekNotAnObject,
    rekInvalidJson, rekOther
  RpcError* = object ## since we're using Result, not an exception subtype
    kind* : RpcErrorKind
    message* : string

#===============================================================================
# CONSTANTS
## properties required of an RPC request
const requestProps* : seq[JsonProp] =
  @[("jsonrpc", @[JString],              true),
    ("id",      @[JNull, JInt, JString], true),
    ("method",  @[JString],              true),
    ("params",  @[JArray, JObject],      false)]
## properties required of an RPC response. `error`'s structure needs to
## be verified separately
const responseProps* : seq[JsonProp] =
  @[("jsonrpc", @[Jstring],              true),
    ("id",      @[JNull, JInt, JString], true),
    ("result",  @[JNull, JBool, JInt, JFloat, JString, JObject, JArray], false),
    ("error",   @[JObject],              false)]
## properties of an RPC response error
const responseErrorProps* : seq[JsonProp] =
  @[("code",    @[JInt],                 true),
    ("message", @[JString],              true),
    ("data",    @[JNull, JBool, JInt, JFloat, JString, JObject, JArray], false)]
## properties of an RPC notification
const notificationProps* : seq[JsonProp] =
  @[("jsonrpc", @[Jstring],              true),
    ("method",  @[JString],              true),
    ("params",  @[JArray, JObject],      false)]


# validation
func validate*(message : JsonNode) : Result[JsonNode, RpcError]
func validateBasic*(message : JsonNode) : Result[JsonNode, RpcError]
func validateRequest*(message : JsonNode) : Result[JsonNode, RpcError]
func validateResponse*(message : JsonNode) : Result[JsonNode, RpcError]
func validateResponseError*(message : JsonNode) : Result[JsonNode, RpcError]
func validateResponseErrorProp*(message : JsonNode) : Result[JsonNode, RpcError]
func validateNotification*(message : JsonNode) : Result[JsonNode, RpcError]

func isRequest*(message : JsonNode) : bool
func isResponse*(message : JsonNode) : bool
func isNotification*(message : JsonNode) : bool

func rpcErr(kind : RpcErrorKind, message : string) : Result[JsonNode, RpcError]
func rpcErr(message : string) : Result[JsonNode, RpcError]
func validateProp(prop : JsonProp, obj : JsonNode) : Result[JsonNode, RpcError]
func validateProps(props : seq[JsonProp], obj : JsonNode) :
     Result[JsonNode, RpcError]
func noInvalidProps(props: seq[string], obj : JsonNode) :
     Result[JsonNode, RpcError]
func noInvalidProps(props: seq[JsonProp], obj : JsonNode) :
     Result[JsonNode, RpcError]
func eitherResultOrError(obj : JsonNode) : Result[JsonNode, RpcError]


#===============================================================================
# EXPORTED PROC IMPLEMENTATIONS
func validate*(message : JsonNode) : Result[JsonNode, RpcError] =
  ## validate an RPC message, returning it if it's valid or else an error
  return validateRequest(message).or(
    validateResponse(message)).or(
    validateNotification message)

func validateBasic*(message : JsonNode) : Result[JsonNode, RpcError] =
  ## validate the properties of an RPC message (is it an object, does it have
  ## a `jsonrpc` key that are universal to request, response, and notification
  if message.kind != JObject:
    return rpcErr(rekNotAnObject, "JSON is not an object: " & $message)
  elif message{"jsonrpc"} != %"2.0":
    return rpcErr("wrong or missing version: " & message{"jsonrpc"}.getStr())
  else:
    return results.ok message

func validateRequest*(message : JsonNode) : Result[JsonNode, RpcError] =
  ## validate an RPC request, returning it if it's valid or else an error
  return validateBasic(message).flatMap(
    (obj : JsonNode) => validateProps(requestProps, obj)).flatMap(
    (obj : JsonNode) => noInvalidProps(requestProps, obj))

func validateResponse*(message : JsonNode) : Result[JsonNode, RpcError] =
  ## validate an RPC response, returning it if it's valid or else an error
  return validateBasic(message).flatMap(
    (obj : JsonNode) => validateProps(responseProps, obj)).flatMap(
    (obj : JsonNode) => noInvalidProps(responseProps, obj)).flatMap(
                        eitherResultOrError)               .flatMap(
                        validateResponseErrorProp)

func validateResponseError*(message : JsonNode) : Result[JsonNode, RpcError] =
  ## validate an RPC response error, returning it if it's valid or else an error
  validateProps(responseErrorProps, message).flatmap(
    (obj : JsonNode) => noInvalidProps(responseErrorProps, obj))

func validateResponseErrorProp*(message : JsonNode) :
     Result[JsonNode, RpcError] =
  ## validate an RPC response's error prop, returning an error if the error
  ## prop exists and is invalid, or the response message otherwise
  if not message.hasKey "error":
    return results.ok message
  else:
    return validateResponseError(message["error"]).map(
      (errIgnore : JsonNode) => message)

func validateNotification*(message : JsonNode) :
     Result[JsonNode, RpcError] =
  ## validate an RPC notification, returning it if it's valid or else an error
  validateProps(notificationProps, message).flatMap(
    (obj : JsonNode) => noInvalidProps(notificationProps, obj))


func isRequest*(message : JsonNode) : bool =
  ## says whether a given JSON object is an RPC request. RPC correctness must
  ## be verified beforehand
  message.hasKey("id") and message.hasKey("params")
func isResponse*(message : JsonNode) : bool =
  ## says whether a given JSON object is an RPC response. RPC correctness must
  ## be verified beforehand
  message.hasKey("id") and not message.hasKey("params")
func isNotification*(message : JsonNode) : bool =
  ## says whether a given JSON object is an RPC notification. RPC correctness
  ## must be verified beforehand
  not message.hasKey("id") and message.hasKey("params")

#===============================================================================
# HELPERS

func rpcErr(kind : RpcErrorKind, message : string) :
     Result[JsonNode, RpcError] =
  return results.err RpcError(kind : kind, message : message)

func rpcErr(message : string) : Result[JsonNode, RpcError] =
  return rpcErr(rekOther, message)

func validateProp(prop : JsonProp, obj : JsonNode) :
     Result[JsonNode, RpcError] =
  if not obj.hasKey(prop.key) and prop.required:
    rpcErr(rekMissingProp, prop.key)
  elif obj.hasKey(prop.key) and
       obj[prop.key].kind notin prop.types:
    rpcErr(rekBadPropType, "key: " & prop.key &
      ", type: " & $(obj[prop.key].kind))
  else:
    return results.ok(obj)

func validateProps(props : seq[JsonProp], obj : JsonNode) :
     Result[JsonNode, RpcError] =
  return props.foldl(a.flatMap((obj : JsonNode) => validateProp(b, obj)),
                     results.ok obj)

func noInvalidProps(props: seq[string], obj : JsonNode) :
     Result[JsonNode, RpcError] =
  for prop in obj.keys:
    if prop notin props:
      return rpcErr(rekInvalidProp, prop)
  return results.ok obj

func noInvalidProps(props : seq[JsonProp], obj : JsonNode) :
     Result[JsonNode, RpcError] =
  let propStrings = props.map(func(p : JsonProp) : string = p.key)
  return noInvalidProps(propStrings, obj)

# response
func eitherResultOrError(obj : JsonNode) : Result[JsonNode, RpcError] =
  if obj.hasKey("result") and obj.hasKey("error"):
    return rpcErr "response has both \"result\" and \"error\" keys"
  elif not (obj.hasKey("result") or obj.hasKey("error")):
    return rpcErr(rekMissingProp, "response has neither result nor error")
  else:
    return results.ok obj
