import types
import json

import rpc

proc minimalLspHeaders*(message : JsonNode) : LspMessage =
  ## Make LSP message from RPC message by adding content length header.
  ## Doesn't validate the RPC.
  return LspMessage(contentLength: len($message), contentType: "",
                    contentStr: $message, content: message)

proc `$`*(message : LspMessage) : string =
  ## puts an `LspMessage` into a string in a sendable format
  result = "Content-Length: " & $message.contentLength & "\r\n"
  if message.contentType != "":
    result &= "Content-Type: " & message.contentType & "\r\n"
  result &= "\r\n"
  result &= message.contentStr

proc `{}`*(message : LspMessage, keys : varargs[string]) : JsonNode =
  message.content{keys}

proc `{}`*(message : LspMessage, key : string) : JsonNode =
  message.content{key}

proc `[]`*(message : LspMessage, key : string) : JsonNode =
  message.content[key]

proc isRequest*(message : LspMessage) : bool =
  message.content.isRequest
proc isResponse*(message : LspMessage) : bool =
  message.content.isResponse
proc isNotification*(message : LspMessage) : bool =
  message.content.isNotification
