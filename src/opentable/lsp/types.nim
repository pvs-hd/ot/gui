from json import JsonNode, JsonParsingError
from rpc import RpcError

type
  LspMessage* = object
    contentLength* : int
    contentType* : string
    content* : JsonNode
    contentStr* : string
  LspErrorKind* = enum
    lekBadSubtype, lekBadHeader, lekBadRpc, lekBadJson, lekParseError,
    lekException, lekOther
  LspError* = object
    message* : string
    case kind* : LspErrorKind:
      of lekBadJson:
        jsonError*: JsonParsingError
      of lekBadRpc:
        rpcError*: RpcError
      of lekException:
        parent*: CatchableError
      else:
        discard
