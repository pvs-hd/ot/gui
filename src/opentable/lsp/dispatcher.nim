import json
from types import LspMessage, LspErrorKind, LspError
import rpc
import parser

export types, parser, rpc

type
  # assigning a subclass object won't work unless this is ref, see
  # https://github.com/nim-lang/Nim/issues/17602
  LspDispatcher* = ref object of RootObj
    receiveMessage* : proc(message : LspMessage) : bool {.nimcall gcsafe.}

method sendMessage*(disp : LspDispatcher, message : LspMessage) :
       bool {.base.} =
  ## Simplest version: echo the message back with `receiveMessage`.
  let f = disp.receiveMessage
  return f(message)
