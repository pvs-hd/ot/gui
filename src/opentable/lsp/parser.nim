##[
Provides a proc to parse LSP messages from strings and ``Stream``s.
]##
import results
from strutils import split, parseInt
import streams
import sugar
import json
import tables
from sequtils import toSeq, map, count

from types import LspMessage, LspErrorKind, LspError
import rpc

export types, rpc

# Just a helper shorthand, not exported
type
  KVPair*  = tuple[key, value : string]
  Headers* = seq[KVPair]
  Alist[A, B] = seq[tuple[key : A, value : B]]


#===============================================================================
# CONSTANTS
## The list of accepted headers for LSP messages.
const lspHeaders* : seq[string] = @["Content-Type", "Content-Length"]


#===============================================================================
# PROCS
proc parseLsp*(message : string) : Result[LspMessage, LspError] {.gcsafe.}
proc lspMessage*(headers : Headers, content : JsonNode, contentStr : string) :
               Result[LspMessage, LspError] {.gcsafe.}
proc parseLspHeaders*(s : string) : Result[Headers, LspError] {.gcsafe.}
proc parseLspHeaders*(s : Stream) : Result[Headers, LspError] {.gcsafe.}
proc validateHeaders*(headers : Headers, contentLength: int) :
                    Result[Headers, LspError] {.gcsafe.}
proc parseLspContent*(s : string) : Result[JsonNode, LspError] {.gcsafe.}
proc parseLspContent*(s : Stream) : Result[JsonNode, LspError] {.gcsafe.}
func wrapRpcError*(err : RpcError) : LspError {.gcsafe.}

proc parseLspHeadersRec(s : Stream, headers : Headers) :
                    Result[Headers, LspError] {.gcsafe.}
proc parseLspHeader(line : string) :
                   Result[tuple[key : string, value : string], LspError]
    {.gcsafe.}
proc readLineCrlf(s : Stream) : Result[string, LspError] {.gcsafe.}
proc hasKey[A, B](ps : Alist[A, B], key : A) : bool {.gcsafe.}
proc assoc[A, B](ps : Alist[A, B], key : A, default : B) : B {.gcsafe.}
proc assoc[A, B](ps : Alist[A, B], key : A) : B {.gcsafe.}

#===============================================================================
# EXPORTED PROCS
proc parseLsp*(message : string) : Result[LspMessage, LspError] {.gcsafe.} =
  ## parses a string into an LSP message. Trailing garbage leads to an error.
  var strm = newStringStream message
  let headers       = parseLspHeaders strm
  let pos           = strm.getPosition()
  strm.close()
  let contentStr = message[pos..^1]
  let contentLength = len contentStr
  let content       = parseLspContent contentStr
  return headers
    .flatMap((hs : Headers) =>
             validateHeaders(hs, contentLength))
    .flatMap((hs : Headers) =>
             content.flatMap((content : JsonNode) =>
                             lspMessage(hs, content, contentStr)))

proc lspMessage*(headers : Headers, content : JsonNode, contentStr : string) :
               Result[LspMessage, LspError] {.gcsafe.} =
  ## creates an LSP message from the given headers and the given body.
  ## Performs no checks on anything.
  return results.ok LspMessage(
    contentLength: parseInt headers.assoc("Content-Length"),
    contentType: headers.assoc("Content-Type", ""),
    content: content, contentStr: contentStr)

proc parseLspHeaders*(s : string) : Result[Headers, LspError] {.gcsafe.} =
  ## parses the headers of an LSP message from a string. Ignores the body and
  ## other trailing junk, but requires the terminating \r\n
  ## Only checks the ``Key: value\r\n`` format, to validate both keys and values
  ## use `validateHeaders<#validateHeaders, Headers, int>`_.
  parseLspHeaders(newStringStream s)

proc parseLspHeaders*(s : Stream) : Result[Headers, LspError] {.gcsafe.} =
  ## Parses the headers of an LSP message from a stream. The stream is left at
  ## what should be the start of the RPC body.
  ## Only checks the ``Key: value\r\n`` format, to validate both keys and values
  ## use `validateHeaders<#validateHeaders, Headers, int>`_.
  var hs : Headers = @[]
  return parseLspHeadersRec(s, hs)

proc validateHeaders*(headers : Headers, contentLength: int) :
                    Result[Headers, LspError] {.gcsafe.} =
  ## Validates the headers of an LSP message, e.g. checking that all keys
  ## be members of `lspHeaders<#lspHeaders>`_, that there are no duplicate keys,
  ## etc.
  for kv in headers:
    if headers.map(kv => kv.key).count(kv.key) > 1:
      return results.err LspError(kind : lekBadHeader,
                                  message : "duplicate header key: " & kv.key)
  for key in headers.map(kv => kv.key):
    if key notin lspHeaders:
      return results.err LspError(kind: lekBadHeader,
                                  message: "unknown header: " & key)
  if not headers.hasKey "Content-Length":
    return results.err LspError(kind : lekBadHeader,
                                message: "missing Content-Length header")
  else:
    let lengthValue = headers.assoc("Content-Length")
    var supposedContentLength : int
    try:
      supposedContentLength = parseInt lengthValue
    except ValueError:
      return results.err(LspError(kind : lekBadHeader, message: "couldn't " &
        "parse Content-Length: " & lengthValue))
    if supposedContentLength != contentLength:
      return results.err(LspError(kind : lekBadHeader,
                                  message: "Content-Length is " &
                                    $supposedContentLength & ", but actual " &
                                    "content length is " & $contentLength))
    else:
      return results.ok headers

proc parseLspContent*(s : string) : Result[JsonNode, LspError] {.gcsafe.} =
  ## Parses and validates the JSON-RPC body of an LSP message. See
  ## `parseLspContent,Stream<#parseLspContent,Stream>`_ for details.
  parseLspContent(newStringStream s)

proc parseLspContent*(s : Stream) : Result[JsonNode, LspError] {.gcsafe.} =
  ## Parses the JSON body of an LSP message. Uses ``json.parseJson`` and
  ## accordingly does not accept any trailing whitespace.
  try:
    return rpc.validate(parseJson s).mapErr(wrapRpcError) # closes stream!
  except JsonParsingError:
    let ex = (ref JsonParsingError)getCurrentException()
    return  Result[JsonNode, LspError].err(
      LspError(kind: lekBadJson, message: "", jsonError: ex[]))

func wrapRpcError*(err : RpcError) : LspError {.gcsafe.} =
  ## wraps an `RpcError` in an `LspError`
  LspError(kind : lekBadRpc, message : "", rpcError : err)

#===============================================================================
# HELPER PROCS

proc parseLspHeadersRec(s : Stream, headers : Headers) :
                       Result[Headers, LspError] {.gcsafe.} =
  ## see `parseLspHeaders<#parseLspHeaders,Stream>`_ for detail
  return s.readLineCrlf().flatMap((line : string) =>
    (if line == "":
       return Result[Headers, LspError].ok headers
     else:
       return parseLspHeader(line)
         .map((kv : KVPair) => headers & @[kv])
         .flatMap((hs : Headers) => parseLspHeadersRec(s, hs))))

proc parseLspHeader(line : string) : Result[KVPair, LspError] {.gcsafe.} =
  ## parses one LSP header, i.e. <key>": "<value>. checks the validity of
  ## <key> and the presence of the delimiter, nothing more
  let parts : seq[string] = line.split(": ", 1)
  if len(parts) < 2:
    return results.err LspError(kind: lekParseError,
                                message: "no \": \" in line")
  else:
    return results.ok (parts[0], parts[1])                              

proc readLineCrlf(s : Stream) : Result[string, LspError] {.gcsafe.} =
  ## read a line from a stream. Return everything up to a CRLF or EOF, or an
  ## LspError if CR or LF are found on their own. Not pure, mutates `s`
  var line = ""
  var done : bool = false
  while not done:
    var c = readChar(s)
    if c == '\r':
      c = readChar(s)
      if c == '\l':
        done = true
      else:
        return results.err(
          LspError(kind: lekParseError, message: "found CR without LF at " &
            "position " & $s.getPosition))
    elif c == '\l':
      return results.err(
        LspError(kind: lekParseError, message: "found LF without CR at " &
          "position " & $s.getPosition))
    elif c == '\0':
      done = true
    else:
      line.add(c)
  return results.ok line

proc hasKey[A, B](ps : Alist[A, B], key : A) : bool {.gcsafe.} =
  for p in ps:
    if p.key == key:
      return true
  return false

proc assoc[A, B](ps : Alist[A, B], key : A, default : B) : B {.gcsafe.} =
  for p in ps:
    if p.key == key:
      return p.value
  return default

proc assoc[A, B](ps : Alist[A, B], key : A) : B {.gcsafe.} =
  for p in ps:
    if p.key == key:
      return p.value
  raise newException(ValueError, "key not found in association list")
