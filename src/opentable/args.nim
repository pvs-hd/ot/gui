from os import paramCount, paramStr
from regex import re, match
from strutils import split, parseInt


#===============================================================================
# TYPES
type
  Args*     = object
    portNo* : int
    database* : string


#===============================================================================
# PROCS
proc parseArguments*(defaultArgs : Args) : Args
proc usage*(defaultArgs : Args) : void

proc processKVArg(args : var Args, nextArg : string) : Args
proc processFlag(args : Args, flag : string) : Args
proc isFlag(arg : string) : bool
proc processArgPair(args : Args, key : string, value : string) : Args

proc setPortNo(args : var Args, value : string) : Args
proc setDatabase(args : var Args, value : string) : Args
proc lookup[K, V](alist : seq[(K, V)], key : K) : V


#===============================================================================
# CONSTANTS
## All possible keys for arguments currently.
const argKeys* :
  seq[(string, proc(args : var Args, value : string) : Args {.nimcall.})] =
  @[("port", setPortNo),
    ("database", setDatabase)]


#===============================================================================
# IMPLEMENTATIONS
proc usage*(defaultArgs : Args) : void =
  ## Print out info about how to start the program, then exit.
  let usageMessage = "Usage: ./opentable [options]\nOptions:" &
    "\n  --port:<n> Listen on port n (default " & $defaultArgs.portNo & ")" &
    "\n  --database:<path> Connect to sqlite database at given path." &
    "\n      Default is " & defaultArgs.database & " (relative to application" &
    "\n      directory)"
  echo usageMessage
  quit 1

proc parseArguments*(defaultArgs : Args) : Args =
  ## Parse program arguments. See `usage<#usage,Args>`_ for acceptable
  ## combinations.
  result = defaultArgs
  case paramCount():
    of 0:
      return result
    else:
      let argc = paramCount()
      for arg in 1..argc:
        let nextArg : string = paramStr arg
        try:
          # e.g. --portNo:5000
          if (match(nextArg, re"--.*")):
            result = processKVArg(result, nextArg)
          # e.g. -f file
          elif (match(nextArg, re"-[^-].*")) and isFlag(nextArg):
            result = processFlag(result, nextArg)
          elif (match(nextArg, re"-[^-].*")) and arg < argc:
            result = processArgPair(result, nextArg, paramStr(arg + 1))
          else:
            usage defaultArgs
        except:
          usage defaultArgs


#===============================================================================
# Procs for processing individual arguments/argument pairs
# --key:value type arguments
proc processKVArg(args : var Args, nextArg : string) : Args =
  if (nextArg[0..1] != "--"):
    raise newException(CatchableError, "expected --key:value, but input" &
      " doesn't begin with --: " & nextArg)
  let withoutDashes : string      = nextArg[2..^1]
  let kv            : seq[string] = withoutDashes.split ':'
  if (len kv) != 2:
    raise newException(CatchableError, "expected --key:value, but input has" &
      " either too many or too few colons: " & nextArg)
  return args

# -f <file> type arguments (so also key-value, but in a different format)
proc processArgPair(args : Args, key : string, value : string) : Args =
  raise newException(CatchableError, "no \"-k v\" -type arguments defined")

# -f type arguments (flags)
proc processFlag(args : Args, flag : string) : Args =
  raise newException(CatchableError, "no flag-type arguments defined")
# currently no flag-type arguments implemented
proc isFlag(arg : string) : bool =
  false

#===============================================================================
# The functions for setting the appropriate value based on the argument's key
proc setPortNo(args : var Args, value : string) : Args =
  args.portNo = parseInt value
  return args
proc setDatabase(args : var Args, value : string) : Args =
  args.database = value
  return args
proc lookup[K, V](alist : seq[(K, V)], key : K) : V =
  for pair in alist:
    if pair[0] == key:
      return pair[1]
  raise newException(CatchableError, "key not found")
