#!/usr/bin/python3
from typing import List
from sys import argv
from pathlib import Path
from shutil import rmtree
from subprocess import PIPE
import subprocess
import os
import sys


#===============================================================================
# GLOBALS
WINDOWS        = sys.platform.startswith("win32")

project_dir    = Path(os.path.dirname(os.path.abspath(__file__)))
web_dir        = Path("web")
svelte_dir     = Path("svelte")
data_dir       = Path("data")
db_dir         = data_dir / Path("database")

exe_name       = Path("opentable.exe" if WINDOWS else "opentable")

db_setup_exe   = Path("databasesetup.exe" if WINDOWS else "databasesetup")
master_db_file = db_dir / Path("test.sqlite")
db_init_csv    = Path("data/tb1_data.csv")

default_task   = "build"


#===============================================================================
# TYPES
class Args:
    def __init__(self, task):
        self.task = task


#===============================================================================
# MAIN
def main():
    args = processArgs(argv)
    if (args == None):
        exit(1)
    else:
        (args.task)()


#===============================================================================
# BUILD TASKS

# setup
def setup():
    setup_database()

def setup_database():
    # compile database setup if not yet present
    if not os.path.exists(data_dir / db_setup_exe):
        nimble_args = ["nimble", "build", str(db_setup_exe.stem)]
        report_command(nimble_args)
        if WINDOWS:
            nimble_process = subprocess.run(args=nimble_args, shell=True)
        else:
            nimble_process = subprocess.run(args=nimble_args)
        # nimble doesn't allow -o option, so we have to manually move it
        os.rename(Path.cwd() / db_setup_exe, data_dir / db_setup_exe)
    # run database setup
    if not os.path.exists(db_dir):
        os.mkdir(db_dir)
    db_setup_args = [str(data_dir / db_setup_exe),
                     str(master_db_file), str(db_init_csv)]
    report_command(db_setup_args)
    if WINDOWS:
        dbsetup_process = subprocess.run(args=db_setup_args, shell=True)
    else:
        dbsetup_process = subprocess.run(args=db_setup_args)

# build
def build():
    clean()
    build_svelte()
    build_nim()
    setup()

def build_svelte():
    # We have all our svelte resources in a subdirectory
    os.chdir(Path.cwd() / svelte_dir)
    setup_smui()
    build_args = ["npm", "run", "build"]
    report_command(build_args)
    if WINDOWS:
        maybe_built = subprocess.run(build_args, shell=True)
    else:
        maybe_built = subprocess.run(build_args)
    if (maybe_built.returncode != 0):
        print("error, Node libraries may not be installed")
        npm_install()
        report_command(build_args)
        if WINDOWS:
            subprocess.run(build_args, shell=True)
        else:
            subprocess.run(build_args)
    os.chdir("..")

# nimble build may ask for confirmation to download a packages.json file,
# so we have to send it a "yes" if it does.
def build_nim():
    nimble_args = ["nimble", "build", str(exe_name.stem)]
    report_command(nimble_args)
    if WINDOWS:
        nimble_process = subprocess.Popen(args=nimble_args, text=True,
                                          stdin=PIPE, shell=True)
    else:
        nimble_process = subprocess.Popen(args=nimble_args, text=True,
                                          stdin=PIPE)
    nimble_process.communicate(input="y\n")

    
# test
def test():
    test_svelte()
    test_nim()

def test_svelte():
    ## runs the svelte test suite. Unlike build_svelte, doesn't run npm install
    ## if something goes wrong, because jest unfortunately also returns a
    ## non-zero status if the tests fail. So, make sure you've run build first.
    test_args = ["npm", "run", "test"]
    os.chdir(Path.cwd() / svelte_dir)
    report_command(test_args)
    if WINDOWS:
        subprocess.run(test_args, shell=True)
    else:
        subprocess.run(test_args)
    os.chdir("..")

def test_nim():
    nimble_args = ["nimble", "test"]
    report_command(nimble_args)
    if WINDOWS:
        subprocess.run(args=nimble_args, shell=True)
    else:
        subprocess.run(args=nimble_args)


# doc
def doc():
    doc_nim()

def doc_nim():
    nimble_args = ["nimble", "gendoc"]
    report_command(nimble_args)
    if WINDOWS:
        nimble_process = subprocess.Popen(args=nimble_args, text=True,
                                          stdin=PIPE, shell=True)
    else:
        nimble_process = subprocess.Popen(args=nimble_args, text=True,
                                          stdin=PIPE)
    nimble_process.communicate(input="y\n")
    
# clean
def clean():
    clean_svelte()
    clean_nim()
    delete_and_report(data_dir / db_setup_exe)
    clean_database()

def clean_svelte():
    delete_and_report(web_dir / "app.js")
    delete_and_report(web_dir / "app.css")
    delete_and_report(web_dir / "smui.css")
    delete_and_report(web_dir / "smui-dark.css")

def clean_nim():
    delete_and_report(exe_name)

def clean_database():
    if (os.path.exists(db_dir)):
        rmtree(db_dir)
        os.mkdir(db_dir)


# wipe - clean() and remove node artifacts
def wipe():
    wipe_svelte()
    clean()

def wipe_svelte():
    delete_and_report(svelte_dir / "package-lock.json")
    node_modules_dir = svelte_dir / "node_modules"
    if (os.path.exists(node_modules_dir)):
        rmtree(node_modules_dir)
        print("deleted: " + str(node_modules_dir))

def print_help():
    usage()

tasks = {
    "setup": setup,
    "setup-database": setup_database,
    "clean-database": clean_database,
    "build": build,
    "build-svelte": build_svelte,
    "build-nim": build_nim,
    "test": test,
    "test-nim": test_nim,
    "test-svelte": test_svelte,
    "doc": doc,
    "clean": clean,
    "clean-svelte": clean_svelte,
    "clean-nim": clean_nim,
    "wipe": wipe,
    "wipe-svelte": wipe_svelte,
    "help": print_help
}


#===============================================================================
# HELPERS
def npm_install():
    npm_args = ["npm", "install"]
    report_command(npm_args)
    if WINDOWS:
        subprocess.run(npm_args, shell=True)
    else:
        subprocess.run(npm_args)

def setup_smui():
    # These are already in clean_svelte, however that isnt necessarily always
    # run when building. These have to be deleted every time.
    delete_and_report(project_dir / web_dir / "smui.css")
    delete_and_report(project_dir / web_dir / "smui-dark.css")
    npm_args = ["npm", "run", "setup-smui"]
    report_command(npm_args)
    if WINDOWS:
        subprocess.run(npm_args, shell=True)
    else:
        subprocess.run(npm_args)

#===============================================================================
# I/O
def processArgs(args) -> Args:
    if len(args) == 1:
        return Args(tasks[default_task])
    elif len(args) == 2:
        f = None
        try:
            f = tasks[args[1]]
            return Args(f)
        except KeyError:
            usage()
            return None
    else:
        usage()
        return None

def usage():
    print("Usage: python3 build.py [TASK]")
    print("Possible tasks: ")
    print("\n".join(map(
        lambda task: task + " [default]" if (task == default_task) else task,
        tasks.keys())))


#===============================================================================
# DIAGNOSTIC/LOGGING
def delete_and_report(file_path):
    if (os.path.exists(file_path)):
        os.remove(file_path)
        print("deleted: " + str(file_path))

def report_command(args : List[str]):
    print(" ".join([">>>"] + args))
    

#===============================================================================
# SCRIPT
main()
